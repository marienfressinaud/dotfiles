# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/.bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/.bin:$PATH"
fi
export PATH

export EDITOR="vim"

# Alias divers
alias ls='ls --indicator-style=slash --group-directories-first --color'
alias ll='ls -l'
alias la='ls -la'
alias grep='grep --color=auto'
alias tree='tree --dirsfirst -C'
alias share='python3 -m http.server 8080'

# Archive extractor, usage: ex <file>
ex ()
{
    if [ -f $1 ] ; then
        case $1 in
            *.tar.bz2)   tar xjf $1   ;;
            *.tar.gz)    tar xzf $1   ;;
            *.bz2)       bunzip2 $1   ;;
            *.rar)       unrar x $1     ;;
            *.gz)        gunzip $1    ;;
            *.tar)       tar xf $1    ;;
            *.tbz2)      tar xjf $1   ;;
            *.tgz)       tar xzf $1   ;;
            *.zip)       unzip $1     ;;
            *.Z)         uncompress $1;;
            *.7z)        7z x $1      ;;
            *)           echo "'$1' cannot be extracted via ex()" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}

# "Incremental history"
# usage : taper quelques lettres de la commande, appuyer sur flèche du haut
# Cela recherche dans l'historique les commandes commançant par les lettres
# tapées.
bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'

# Système de signets
# Code from https://linuxfr.org/users/robin--2/journaux/des-bookmarks-dans-mon-terminal
function ccd() {
    if [ -r ~/.config/signets/save$1 ]
    then
        cd "$(cat ~/.config/signets/save$1)"
    else
        echo >&2 "Le signet $1 n'existe pas"
    fi
}
function x() {
    mkdir -p ~/.config/signets
    pwd > ~/.config/signets/save$1
    pwd > ~/.config/signets/save
}
function _ccd() {
    local cur prev opts
    cur="${COMP_WORDS[COMP_CWORD]}"
    COMPREPLY=( $(shopt -s nullglob; echo $HOME/.config/signets/save${cur}* ) )
    COMPREPLY=( ${COMPREPLY[@]#$HOME/.config/signets/save} )
    return 0
}
complete -F _ccd ccd

# Corrige l'autocompletion de la commande SSH
_ssh()
{
    local cur prev opts
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"
    opts=$(grep '^Host' ~/.ssh/config ~/.ssh/config.d/* 2>/dev/null | grep -v '[?*]' | cut -d ' ' -f 2-)

    COMPREPLY=( $(compgen -W "$opts" -- ${cur}) )
    return 0
}
complete -F _ssh ssh

# Configuration de FZF
[ -f ~/.fzf.bash ] && source ~/.fzf.bash

# Désactive le "Software Flow Control"
# Voir http://unix.stackexchange.com/questions/72086/ctrl-s-hang-terminal-emulator
# Et https://stackoverflow.com/a/25391867
[[ $- == *i* ]] && stty -ixon

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# User specific aliases and functions
if [ -d ~/.bashrc.d ]; then
    for rc in ~/.bashrc.d/*; do
        if [ -f "$rc" ]; then
            . "$rc"
        fi
    done
fi

unset rc
