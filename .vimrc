""""""""""""""""""""""""""""""""""""""""
"
" A very basic .vimrc for personnal purposes only.
" Comments have been inspired by https://github.com/amix/vimrc
"
" Additional configuration files (*.vim) can be placed under ~/.vim/plugin
"
""""""""""""""""""""""""""""""""""""""""

""""""""""""""""""""""""""""""""""""""""
" => General
""""""""""""""""""""""""""""""""""""""""
" Disable Vim compatible mode
set nocompatible

" filetype detections
filetype on
filetype plugin on
filetype indent on

" Set to auto read when a file is changed from the outside
set autoread

" Remap leader key
let mapleader = ","

" Enable mouse
" set mouse=a

""""""""""""""""""""""""""""""""""""""""
" => UI
""""""""""""""""""""""""""""""""""""""""
" Always show current position
set ruler

" Show line numbers
set number

" Show relative line numbers
set relativenumber

" Highlight search results
set hlsearch

" Force case-sensitive search
set noignorecase
set nosmartcase

" Start searching when typing
set incsearch

" Add a preview when search/replace
if has('nvim')
  set inccommand=nosplit
endif

" Underline current search in visual mode
highlight incsearch cterm=underline

" Automatic word wrapping
set textwidth=79
au BufNewFile,BufRead *.md set textwidth=0

" Show matching brackets when cursor is over them
set showmatch

" Show invisible characters
set list listchars=nbsp:·,tab:¬·,trail:¤,extends:▶,precedes:◀

""""""""""""""""""""""""""""""""""""""""
" => Folding
""""""""""""""""""""""""""""""""""""""""
set foldmethod=indent
set foldlevelstart=1

""""""""""""""""""""""""""""""""""""""""
" => Colors and fonts
""""""""""""""""""""""""""""""""""""""""
" Enable syntax highlighting
syntax on

" Show extra-whitespaces in red
highlight ExtraWhitespace ctermbg=red guibg=red
autocmd Syntax * syn match ExtraWhitespace /\s\+$\| \+\ze\t/

" Show a gutter on the right at 80 chars
highlight ColorColumn ctermbg=0 guibg=lightgrey
set colorcolumn=80,90

""""""""""""""""""""""""""""""""""""""""
" => Files, backups and undo
""""""""""""""""""""""""""""""""""""""""
" Turn backup off, since most stuff is in Git
set nobackup
set nowb
set noswapfile

""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
""""""""""""""""""""""""""""""""""""""""
" Use spaces instead of tabs
set expandtab

" Use tabs or spaces smartly
set smarttab

" 1 tab = 4 spaces by default
set tabstop=4
set shiftwidth=4

" Improve behaviour when joining lines (remove comment char and avoid double
" spaces).
set formatoptions+=j
set nojoinspaces

""""""""""""""""""""""""""""""""""""""""
" => Status line
""""""""""""""""""""""""""""""""""""""""
" Always show the status line
set laststatus=2

""""""""""""""""""""""""""""""""""""""""
" => Moving around
""""""""""""""""""""""""""""""""""""""""

""""""""""""""""""""""""""""""""""""""""
" => Splitting
""""""""""""""""""""""""""""""""""""""""
" Split navigation with ctrl+h/j/k/l
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" More natural splitting
set splitbelow
set splitright

""""""""""""""""""""""""""""""""""""""""
" => Useful mappings
""""""""""""""""""""""""""""""""""""""""
" Make Y great again
nmap Y y$

" Make jk working as <Esc> key
imap jk <Esc>

" Make capital U working as ctrl+R (so it reverses u)
nnoremap U <C-R>

" Insert blank lines without entering into insert mode with leader+o/O
nnoremap <leader>o o<Esc>k
nnoremap <leader>O O<Esc>j

" Delete the whole content of the current line
nnoremap do 0D

" You should not use arrows to move
nnoremap <Up> <Nop>
inoremap <Up> <Nop>
nnoremap <Down> <Nop>
inoremap <Down> <Nop>
nnoremap <Right> <Nop>
inoremap <Right> <Nop>
nnoremap <Left> <Nop>
inoremap <Left> <Nop>

""""""""""""""""""""""""""""""""""""""""
" => Digraphs mappings
" Insert special characters in Insert
" mode with CTRL+k
""""""""""""""""""""""""""""""""""""""""
" Hyphenation Point
dig :. 8231

""""""""""""""""""""""""""""""""""""""""
" => Python section
""""""""""""""""""""""""""""""""""""""""
let g:pymode_python = 'python3'
let python_highlight_all=1

""""""""""""""""""""""""""""""""""""""""
" => Source configuration files on save
""""""""""""""""""""""""""""""""""""""""
augroup configurationFiles
  autocmd BufWritePost .vimrc source %
augroup END

""""""""""""""""""""""""""""""""""""""""
" => Configure vim-plug
" https://github.com/junegunn/vim-plug
"
" Basic usage:
" Download https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
" into ".vim/autoload" folder.
"
" Install with PlugInstall [name ...]
" Update or install with PlugUpdate [name ...]
""""""""""""""""""""""""""""""""""""""""
call plug#begin('~/.vim/plugged')
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

Plug 'airblade/vim-gitgutter'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

Plug 'valloric/youcompleteme'

Plug 'scrooloose/nerdtree'

Plug 'w0rp/ale'

Plug 'captbaritone/better-indent-support-for-php-with-html'
Plug 'StanAngeloff/php.vim'
Plug 'nelsyeung/twig.vim'
call plug#end()

""""""""""""""""""""""""""""""""""""""""
" => Configure FZF
" https://github.com/junegunn/fzf.vim
""""""""""""""""""""""""""""""""""""""""
map <c-p> :Files<CR>
map <c-m> :History<CR>
map <c-g> :Ag<CR>

""""""""""""""""""""""""""""""""""""""""
" => Configure Airline
" https://github.com/vim-airline/vim-airline
""""""""""""""""""""""""""""""""""""""""
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#fnamemod = ':t'

""""""""""""""""""""""""""""""""""""""""
" => Configure NerdTree
" https://github.com/scrooloose/nerdtree
""""""""""""""""""""""""""""""""""""""""
let NERDTreeMouseMode = 2
map <leader>n :NERDTreeToggle<CR>
let NERDTreeIgnore = ['\.pyc$', '^__pycache__$']

""""""""""""""""""""""""""""""""""""""""
" => Configure ALE (Asynchronous Lint Engine)
" https://github.com/w0rp/ale
""""""""""""""""""""""""""""""""""""""""
let g:ale_lint_on_text_changed = 'never'
let g:ale_sign_column_always = 1
let b:ale_fixers = ['prettier', 'eslint']
let g:ale_php_phpcs_standard = 'PSR12'
let g:ale_pattern_options = {'.*\.html\.twig$': {'ale_enabled': 0}}
nmap <leader>a :ALENext<cr>
