# Marien’s dotfiles

These are my personal dotfiles. I try to keep them as simple as possible.

## Installation

Clone the repository on your computer (I usually place this repo under a
`~/Applications` folder):

```console
$ mkdir ~/Applications
$ git clone git@framagit.org:marienfressinaud/dotfiles.git ~/Applications/dotfiles
```

Then, create symbolic links to your system files:

```console
$ mkdir -p ~/.vim/autoload
$ rm ~/.bashrc ~/.vimrc ~/.gitconfig ~/.vim/autoload/plug.vim
$ ln -s ~/Applications/dotfiles/.bashrc ~/.bashrc
$ ln -s ~/Applications/dotfiles/.vimrc ~/.vimrc
$ ln -s ~/Applications/dotfiles/.gitconfig ~/.gitconfig
$ ln -s ~/Applications/dotfiles/.vim/autoload/plug.vim ~/.vim/autoload/plug.vim
```

Create a folder to store additionnal bashrc scripts:

```console
$ mkdir ~/.bashrc.d
```

Source your (new) `.bashrc`:

```console
$ . ~/.bashrc
```

Open Vim, and install the plugins:

```vim
:PlugUpgrade
:PlugInstall
```

## Compile YouCompleteMe

Install the dependencies:

```console
$ # Fedora
$ sudo dnf install cmake gcc-c++ make python3-devel
$ # Debian
$ sudo apt install build-essential cmake python3-dev
```

Go to the YouCompleteMe folder:

```console
$ cd ~/.vim/plugged/youcompleteme
```

Compile the plugin:

```console
$ ./install.py
```

## FZF and The Silver Searcher

To improve the performance of FZF, install [The Silver Searcher](https://github.com/ggreer/the_silver_searcher).

Then, create a `bashrc` file to configure FZF:

```console
echo "export FZF_DEFAULT_COMMAND=\"ag --hidden --ignore .git -g ''\"" > ~/.bashrc.d/the-silver-searcher.sh
```

## Liquidprompt

Install liquidprompt:

```console
$ git clone --branch stable https://github.com/nojhan/liquidprompt.git ~/Applications/liquidprompt
```

And create a `bashrc` file to load it when you open a shell:

```console
$ echo '[[ $- = *i* ]] && source ~/Applications/liquidprompt/liquidprompt' > ~/.bashrc.d/liquidprompt.sh
$ . ~/.bashrc
```

## Install Docker

For the full instructions, go to [the Docker documentation](https://docs.docker.com/engine/install/).

For Fedora:

```console
$ sudo dnf -y install dnf-plugins-core
$ sudo dnf-3 config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
$ sudo dnf install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

Start and enable the service:

```console
$ sudo systemctl enable --now docker
```

Add your user to the Docker group:

```console
$ sudo usermod -aG docker $USER
```

Log out and log back with your user, or run the command:

```console
$ newgrp docker
```
